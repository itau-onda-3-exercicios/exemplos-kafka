package br.com.itau.consumer;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.converter.RecordMessageConverter;
import org.springframework.kafka.support.converter.StringJsonMessageConverter;
import org.springframework.messaging.handler.annotation.Payload;

@SpringBootApplication
public class ConsumerApplication {

	@Autowired
	KafkaTemplate<Object, Object> kafkaTemplate;
	
	Logger logger = LoggerFactory.getLogger(ConsumerApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(ConsumerApplication.class, args);
	}
	
	@Bean
	public RecordMessageConverter converter() {
		return new StringJsonMessageConverter();
	}
	
	@KafkaListener(id="pedidos", topics="PagamentosPendentes")
	public void consumir(@Payload Pagamento pagamento) {
		pagamento.setStatus(getRandomBoolean());
		kafkaTemplate.send("PagamentosProcessados", pagamento);
	}
	
	private boolean getRandomBoolean() {
	    Random random = new Random();
	    return random.nextBoolean();
	}	
}
