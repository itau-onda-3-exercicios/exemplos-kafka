Alta disponibilidade
https://kafka.apache.org/

Procedimentos
Baixando e descompactando o pacote
wget http://mirror.nbtelecom.com.br/apache/kafka/2.1.0/kafka_2.11-2.1.0.tgz
tar -xzf kafka_2.11-2.1.0.tgz

Executando o servidor
bin/zookeeper-server-start.sh config/zookeeper.properties
bin/kafka-server-start.sh config/server.properties

bin/kafka-topics.sh --list --zookeeper localhost:2181
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test –from-beginning
bin/kafka-topics.sh --delete --zookeeper localhost:2181 --topic test


git clone https://gitlab.com/lmatayoshi/demo-devops.git

cd /tmp/
mkdir servicos
cd servicos/
wget http://10.162.109.222/kafka.tgz
tar -xzf kafka.tgz 
cd kafka_2.11-2.1.0/




copiar os html para /usr/share;nginx/html

adicionar a seguinte configuração

       # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;
         
        location / {
              add_header 'Access-Control-Allow-Origin' '*';
              add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
        }
        location /send {
  	      proxy_pass http://localhost:8080/send/; 
        }


